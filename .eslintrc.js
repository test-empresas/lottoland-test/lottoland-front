module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  plugins: [],
  // add your custom rules here
  ignorePatterns: ['**/plugins/*'],
  rules: {
    'indent': 'off',
    'no-tabs': 'off',
    'no-console': 'off',
    'no-irregular-whitespace': 'off',
    'quote-props': 'off',
    'padded-blocks': 'off',
    'operator-linebreak': 'off',
    'space-before-function-paren': 'off',
    'vue/singleline-html-element-content-newline': 'off',
    'vue/html-end-tags': 'off',
    'vue/html-indent': 'off',
    'vue/html-self-closing': 'off',
    'vue/html-closing-bracket-newline': 'off',
    'vue/multiline-html-element-content-newline': 'off'
  }
}
