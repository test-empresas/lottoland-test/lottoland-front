export default function ({
    $axios, store
  }) {
    $axios.interceptors.request.use(
      request => {
        request.headers['Accept'] = 'application/json',
        request.headers['Content-Type'] = 'application/json'
        if(request.url != '/token') {
            request.headers['Token'] = store.state.app.token
        }
        return request
      },
      error => {
          console.error(error)
      }
    )
  }