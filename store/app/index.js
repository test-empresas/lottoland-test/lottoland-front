import * as mutations from './mutations'
import * as actions from './actions'

export default {
  namespaced: true,
  state () {
    return {
      token: undefined
    }
  },
  modules: {},
  mutations,
  actions
}
