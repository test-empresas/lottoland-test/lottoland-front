export const SET_STATISTICS = (state, value) => {
    state.statistics = value
}

export const ADD = (state, value) => {
    state.statistics.push(value)
}

export const DELETE = (state, key) => {
    state.statistics = state.statistics.filter(k => k !== key)
    delete state._[key]
}
