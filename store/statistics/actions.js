export const setStatistics = ({ commit, dispatch }, payload) => {
    return new Promise((resolve) => {
        dispatch('empty')
        commit('ADD', {
            total_rounds: payload.total_rounds,
            total_first_win: payload.total_first_win,
            total_second_win: payload.total_second_win,
            total_draw: payload.total_draw
        })
        resolve()
    })
}

export const empty = ({ dispatch, state }) => {
    return new Promise((resolve) => {
        if (!state.statistics.length) {
            resolve()
        }
        Promise.all([
            state.statistics.map((key) => {
                return dispatch('deleteItem', key)
            })
        ]).then(() => {
            resolve()
        })
    })
}

export const deleteItem = ({ commit }, key) => {
    return new Promise((resolve) => {
        commit('DELETE', key)
        resolve()
    })
}
