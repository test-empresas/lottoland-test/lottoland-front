import Vuex from 'vuex'
import app from './app'
import game from './game'
import statistics from './statistics'

const store = () => new Vuex.Store({
    modules: {
        app,
        game,
        statistics
    }
})

export default store
