export const SET_RESULT = (state, value) => {
    state.gameResult = value
}

export const ADD = (state, value) => {
    state.gameResult.push(value)
}

export const DELETE = (state, key) => {
    state.gameResult = state.gameResult.filter(k => k !== key)
    delete state._[key]
}
