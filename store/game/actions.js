export const setResult = ({ dispatch }, array) => {
    return new Promise((resolve) => {
        dispatch('empty')
        array.forEach(payload => dispatch('addItem', payload))
        resolve()
    })
}

export const addItem = ({ commit }, payload) => {
    return new Promise((resolve) => {
        commit('ADD', {
            id: payload.id,
            first: payload.action_first_player,
            second: payload.action_second_player,
            result: payload.result
        })
        resolve()
    })
}

export const deleteItem = ({ commit }, key) => {
    return new Promise((resolve) => {
        commit('DELETE', key)
        resolve()
    })
}

export const empty = ({ dispatch, state }) => {
    return new Promise((resolve) => {
        if (!state.gameResult.length) {
            resolve()
        }
        Promise.all([
            state.gameResult.map((key) => {
                return dispatch('deleteItem', key)
            })
        ]).then(() => {
            resolve()
        })
    })
}
